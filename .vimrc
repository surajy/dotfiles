inoremap jk <ESC>
"" let mapleader = ""
syntax enable
set number
set noswapfile
set hlsearch
set ignorecase
set incsearch
set relativenumber
set wildmenu
set tabstop=4
set shiftwidth=4
set expandtab
set laststatus=2
call plug#begin()
Plug 'frazrepo/vim-rainbow'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'neoclide/coc.nvim', { 'branch': 'release' }
let g:coc_global_extensions = [
  \ 'coc-tsserver'
  \ ]
Plug 'junegunn/fzf.vim'
Plug 'preservim/nerdtree'
call plug#end()

" mappings
"" map <leader>tn :tabnext<cr>
"" map <leader>tp :tabprevious<cr>
"" map <leader>to :tabonly<cr>
"" map <leader>tc :tabclose<cr>
"" map <leader>tm :tabmove<cr>
"" map <leader>tt :tabnew<cr>

nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
